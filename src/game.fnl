(local palette "ffffff6df7c111adc1606c813934571e88755bb361a1e55af7e476f99252cb4d686a3771c92464f48cb6f7b69e9b9c82")
(local p "B7094CA01A58892B64723C705C4D7D455E892E6F951780A10091AD1780A12E6F95455E895C4D7D723C70892B64A01A58")
(var rainbow 0)
(fn load-palette [palette]
 (for [i 0 15] 
  (local r (tonumber (string.sub palette (+ (* i 6) 1) (+ (* i 6) 2)) 16))
  (local g (tonumber (string.sub palette (+ (* i 6) 3) (+ (* i 6) 4)) 16))
  (local b (tonumber (string.sub palette (+ (* i 6) 5) (+ (* i 6) 6)) 16))
  (poke (+ 0x3FC0 (* i 3) 0) r)
  (poke (+ 0x3FC0 (* i 3) 1) g)
  (poke (+ 0x3FC0 (* i 3) 2) b)))


(fn print-fancy [text x y tcol bcol scale width]
 (for [i -1 1]
  (for [j -1 1]
   (print text (+ x i) (+ y j) bcol false scale)))
 (print text x y tcol false scale)
 
 (for [i (- x 1) (+ x 20 (* (# text) (* 5 scale)))]
  (for [j (- y 1) (+ y 3 (* 5 scale))]
   (if (= bcol (pix i j))
   (pix i j (/ (+ i j rainbow) 4)))))   
)

(fn _G.TIC []
 (load-palette p)
 (cls 7)
 (local scale 2)
 (local char-width 5)
 (local char-height 5)
 (local screen-width 240)
 (local screen-height 136)
 (local text "#4 Fancy Text")
 (local x (- (/ screen-width 2)
             (* (# text) char-width)))
 (local y (- (/ screen-height 2)
             (/ char-height 2)))  
 
 (print-fancy text x y 12 13 2)
 
 (set rainbow (+ rainbow 1))

)

